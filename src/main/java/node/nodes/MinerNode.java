package main.java.node.nodes;

public interface MinerNode {
    void generateNewBlock();
    long getHashPower();
}
