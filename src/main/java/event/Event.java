package main.java.event;

public interface Event {
    public void execute();
}
